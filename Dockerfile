FROM php:8.1-fpm as builder

WORKDIR /var/www/shops-data-store

COPY docker/php/php.ini /usr/local/etc/php/
COPY . .
COPY .env.prod .env

RUN apt update \
    && apt install -y  \
      zlib1g-dev  \
      g++  \
      libicu-dev  \
      zip  \
      libzip-dev \
      libpq-dev \
    && docker-php-ext-install  \
      intl  \
      opcache  \
      pdo  \
      pdo_pgsql \
      pgsql \
    && pecl install apcu \
    && docker-php-ext-enable apcu \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip \
    && rm -rf /var/www/html \
    && rm -rf /var/www/shops-data-store/docker

COPY --from=composer:2.2.5 /usr/bin/composer /usr/bin/composer

RUN composer --working-dir=/var/www/shops-data-store --no-dev install

# Build development stage
FROM php:8.1-fpm

COPY --from=builder / /

WORKDIR /var/www/shops-data-store

RUN curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
    && apt-get update \
    && apt-get install --yes git unzip \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    \
    && echo "xdebug.remote_connect_back=1" >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_enable=1" >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    \
    && sed -i 's/session.cookie_secure = 1/session.cookie_secure = 0/' $PHP_INI_DIR/php.ini \
    \
    && git config --global user.email "example@example.com" \
    && git config --global user.name "Example Example"
