# Welcome to shops-data-store!

This is a simple API app for importing shops data from csv to Postgres and expose external API 
for getting and updating them. App was written thanks to Symfony 6 with PHP 8.


## Requirements

- Docker
- Docker Compose
- Some free space on your disk

## Installation

### Run in prod mode:

   `./run.sh`

### Run in dev mode:

   `./run.sh --dev`

#### After install dev mode you can run test with command:

   `./run.sh --test`

## Usage

   `./command.sh import:csv-shops-data <file>`

## API documentation

	http://172.55.0.4/api
