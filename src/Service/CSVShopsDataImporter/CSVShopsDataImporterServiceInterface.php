<?php

namespace App\Service\CSVShopsDataImporter;

interface CSVShopsDataImporterServiceInterface
{
    public function importFromFile(string $filePath): void;
}
