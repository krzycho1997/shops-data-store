<?php

namespace App\Service\CSVShopsDataImporter\CSVReader;

use App\Exception\FileNotExistException;
use App\Service\CSVShopsDataImporter\DTO\CSVShopDataDTO;
use Generator;
use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\Reader;
use League\Csv\Statement;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class LeagueCSVReader implements CSVReader
{
    /**
     * @inheritDoc
     *
     * @throws UnknownProperties
     * @throws InvalidArgument
     * @throws Exception
     * @throws FileNotExistException
     */
    public function read(string $filePath): Generator
    {
        $stream = @fopen($filePath, 'r');

        if (!$stream) {
            throw new FileNotExistException();
        }

        $csv = Reader::createFromStream($stream);
        $csv->setDelimiter(',');
        $csv->setHeaderOffset(0);

        $stmt = Statement::create();

        $records = $stmt->process($csv);

        foreach ($records as $record) {
            yield new CSVShopDataDTO($record);
        }
    }
}
