<?php

namespace App\Service\CSVShopsDataImporter\CSVReader;

use App\Service\CSVShopsDataImporter\DTO\CSVShopDataDTO;
use Generator;

interface CSVReader
{
    /**
     * @return Generator<CSVShopDataDTO>
     */
    public function read(string $filePath): Generator;
}
