<?php

namespace App\Service\CSVShopsDataImporter;

use App\Service\CSVShopsDataImporter\CSVReader\CSVReader;
use App\Service\CSVShopsDataImporter\DataPersister\DataPersister;

class CSVShopsDataImporterService implements CSVShopsDataImporterServiceInterface
{
    public function __construct(
        private CSVReader $CSVReader,
        private DataPersister $dataPersister,
    )
    {
    }

    public function importFromFile(string $filePath): void
    {
        $records = $this->CSVReader->read($filePath);
        $this->dataPersister->persist($records);
    }
}
