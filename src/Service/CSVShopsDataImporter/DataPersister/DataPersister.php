<?php

namespace App\Service\CSVShopsDataImporter\DataPersister;

use App\Service\CSVShopsDataImporter\DTO\CSVShopDataDTO;
use Generator;

interface DataPersister
{
    /** @param Generator<CSVShopDataDTO> $data */
    public function persist(Generator $data): void;
}
