<?php

namespace App\Service\CSVShopsDataImporter\DataPersister;

use App\Service\CSVShopsDataImporter\DTO\Mapper\CSVShopDataDTOMapper;
use Doctrine\ORM\EntityManagerInterface;
use Generator;

class PostgresDataPersister implements DataPersister
{
    private const BATCH_SIZE = 1000;

    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /** @inheritDoc */
    public function persist(Generator $data): void
    {
        $i = 1;
        foreach ($data as $CSVShopDataDTO) {
            $shop = CSVShopDataDTOMapper::mapToShopEntity($CSVShopDataDTO);
            $this->entityManager->persist($shop);

            if (($i++ % self::BATCH_SIZE) === 0) {
                $this->flushAndClearData();
                $i = 1;
            }
        }

        $this->flushAndClearData();
    }

    private function flushAndClearData(): void
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
