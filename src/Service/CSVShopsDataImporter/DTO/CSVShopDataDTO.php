<?php

namespace App\Service\CSVShopsDataImporter\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class CSVShopDataDTO extends DataTransferObject
{
    public ?int $id = null;

    public ?string $name = null;

    public ?string $city = null;

    public ?string $street = null;

    public ?string $phone = null;

    public ?string $postalCode = null;

    public ?string $salesman = null;
}
