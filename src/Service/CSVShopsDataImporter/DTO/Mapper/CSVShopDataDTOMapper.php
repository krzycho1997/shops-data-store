<?php

namespace App\Service\CSVShopsDataImporter\DTO\Mapper;

use App\Entity\Shop;
use App\Service\CSVShopsDataImporter\DTO\CSVShopDataDTO;

class CSVShopDataDTOMapper
{
    public static function mapToShopEntity(CSVShopDataDTO $CSVShopDataDTO): Shop
    {
        return (new Shop())
            ->setId($CSVShopDataDTO->id)
            ->setName($CSVShopDataDTO->name)
            ->setCity($CSVShopDataDTO->city)
            ->setStreet($CSVShopDataDTO->street)
            ->setPhone($CSVShopDataDTO->phone)
            ->setPostalCode($CSVShopDataDTO->postalCode)
            ->setSalesman($CSVShopDataDTO->salesman);
    }
}
