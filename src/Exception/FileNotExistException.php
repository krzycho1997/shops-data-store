<?php

namespace App\Exception;

use Exception;

class FileNotExistException extends Exception
{
    /** @var string $message */
    protected $message = 'File not exist in given path!';
}
