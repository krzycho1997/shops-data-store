<?php

namespace App\Command;

use App\Exception\FileNotExistException;
use App\Service\CSVShopsDataImporter\CSVShopsDataImporterServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

#[AsCommand(
    name: 'import:csv-shops-data',
    description: 'Import CSV shops data to database',
)]
class ImportShopsDataCSVCommand extends Command
{
    public function __construct(
        private CSVShopsDataImporterServiceInterface $CSVShopsDataImporterService,
        string $name = null
    )
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('filePath', InputArgument::REQUIRED, 'Path to CSV file data');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $symfonyStyle = new SymfonyStyle($input, $output);

        $filePath = $input->getArgument('filePath');

        try {
            $this->CSVShopsDataImporterService->importFromFile($filePath);
        } catch (FileNotExistException $exception) {

            $symfonyStyle->error($exception->getMessage() . "wrong path: {$filePath}");

            return Command::FAILURE;
        } catch (Throwable $exception) {
            $symfonyStyle->error($exception->getMessage());

            return Command::FAILURE;
        }

        $symfonyStyle->success('Shops data successfully imported to database!');

        return Command::SUCCESS;
    }
}
