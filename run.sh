#!/usr/bin/env bash

compose() {
  docker-compose exec php "$@"
}

run() {
  docker run -d --net shops-data-store-net "$@"
}

if [[ "${1}" == '--dev' ]]; then

  cp .env.local .env

  USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose up -d

  compose composer install

  compose php bin/console doctrine:database:create -n --if-not-exists

  compose php bin/console doctrine:migrations:migrate -n --if-not-exists

elif [ "${1}" == '--test' ]; then

  compose ./vendor/bin/phpunit
else

  docker volume create --name shops-data-store-volume
  docker volume create --name shops-data-store-project

  docker network create \
    --driver=bridge shops-data-store-net \
    --subnet=172.55.0.0/28

  docker build --target builder -t shops-data-store-php .

  run --ip 172.55.0.2 -v shops-data-store-volume:/var/lib/postgresql --name shops-data-store-database -p 5432 -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -e POSTGRES_DB=shops_data_store postgres:13-alpine
  run --ip 172.55.0.3 -u ${UID-1000}:${GID-1000} -v shops-data-store-project:/var/www/shops-data-store --name shops-data-store-php -p 9000 shops-data-store-php
  run --ip 172.55.0.4 -v shops-data-store-project:/var/www/shops-data-store -v "${PWD}"/docker/nginx/prod/default.conf:/etc/nginx/conf.d/default.conf --name shops-data-store-nginx -p 80 nginx:stable-alpine

fi
