<?php

namespace App\Tests\Helper;

use Doctrine\ORM\EntityManagerInterface;

trait WithEntityManager
{
    protected ?EntityManagerInterface $entityManager;

    protected function loadEntityManager(): void
    {
        $this->entityManager = self::bootKernel()
            ->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function closeEntityManager(): void
    {
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
