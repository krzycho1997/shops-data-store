<?php

namespace App\Tests\Helper;

use App\Entity\Shop;
use App\Service\CSVShopsDataImporter\DTO\CSVShopDataDTO;
use App\Service\CSVShopsDataImporter\DTO\Mapper\CSVShopDataDTOMapper;
use Generator;

trait WithShopsProvider
{
    /**
     * @return CSVShopDataDTO[]
     */
    protected function getCSVShopDataDTOArray(): array
    {
        $CSVShopDataDTO_1 = new CSVShopDataDTO();
        $CSVShopDataDTO_1->id = 1;
        $CSVShopDataDTO_1->name = 'Store 1';
        $CSVShopDataDTO_1->city = 'New Carley';
        $CSVShopDataDTO_1->street = 'Ernser Haven 1';
        $CSVShopDataDTO_1->phone = '+1 (320) 352-7909';
        $CSVShopDataDTO_1->postalCode = '20721';
        $CSVShopDataDTO_1->salesman = 'Price Beatty';

        $CSVShopDataDTO_2 = new CSVShopDataDTO();
        $CSVShopDataDTO_2->id = 2;
        $CSVShopDataDTO_2->name = 'Store 2';
        $CSVShopDataDTO_2->city = 'McClureview';
        $CSVShopDataDTO_2->street = 'Durgan Wells 2';
        $CSVShopDataDTO_2->phone = '+1 (410) 657-5305';
        $CSVShopDataDTO_2->postalCode = '45114-1113';
        $CSVShopDataDTO_2->salesman = 'Moses Armstrong';

        $CSVShopDataDTO_3 = new CSVShopDataDTO();
        $CSVShopDataDTO_3->id = 3;
        $CSVShopDataDTO_3->name = 'Store 3';
        $CSVShopDataDTO_3->city = 'New Wittingfort';
        $CSVShopDataDTO_3->street = 'Schoen Center 6';
        $CSVShopDataDTO_3->phone = '+1-341-437-0782';
        $CSVShopDataDTO_3->postalCode = '29669-9212';
        $CSVShopDataDTO_3->salesman = 'Vivian Gutkowski';

        return [
            $CSVShopDataDTO_1,
            $CSVShopDataDTO_2,
            $CSVShopDataDTO_3,
        ];
    }

    /**
     * @return Generator<CSVShopDataDTO>
     */
    protected function getGeneratorWithCSVShopDataDTOs(): Generator
    {
        $shopsDTOs = $this->getCSVShopDataDTOArray();

        foreach ($shopsDTOs as $shopDataDTO) {
            yield $shopDataDTO;
        }
    }

    /**
     * @return Shop[]
     */
    protected function getShopEntitiesArray(): array
    {
        return array_map(
            fn(CSVShopDataDTO $CSVShopDataDTO)
                => CSVShopDataDTOMapper::mapToShopEntity($CSVShopDataDTO),
            $this->getCSVShopDataDTOArray()
        );
    }
}
