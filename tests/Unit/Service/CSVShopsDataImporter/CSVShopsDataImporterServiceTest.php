<?php

namespace App\Tests\Unit\Service\CSVShopsDataImporter;

use App\Service\CSVShopsDataImporter\CSVReader\CSVReader;
use App\Service\CSVShopsDataImporter\CSVShopsDataImporterService;
use App\Service\CSVShopsDataImporter\DataPersister\DataPersister;
use App\Tests\Helper\WithShopsProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;

class CSVShopsDataImporterServiceTest extends TestCase
{
    use WithShopsProvider;

    private CSVReader|Stub $CSVReaderStub;

    private DataPersister|MockObject $dataPersisterMock;

    private CSVShopsDataImporterService $CSVShopsDataImporterService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->CSVReaderStub = $this->createStub(CSVReader::class);
        $this->dataPersisterMock = $this->createMock(DataPersister::class);

        $this->CSVShopsDataImporterService = new CSVShopsDataImporterService($this->CSVReaderStub, $this->dataPersisterMock);
    }

    public function testShouldImportFile(): void
    {
        //given
        $filePath = 'tests/resources/test_stores.csv';
        $shopsDataDTOs = $this->getGeneratorWithCSVShopDataDTOs();

        $this->CSVReaderStub
            ->method('read')
            ->with($filePath)
            ->willReturn($shopsDataDTOs);

        $this->dataPersisterMock
            ->expects($this->exactly(1))
            ->method('persist')
            ->with($shopsDataDTOs);

        //when
        $this->CSVShopsDataImporterService->importFromFile($filePath);
    }
}
