<?php

namespace App\Tests\Unit\Service\CSVShopsDataImporter\CSVReader;

use App\Service\CSVShopsDataImporter\CSVReader\LeagueCSVReader;
use App\Tests\Helper\WithShopsProvider;
use Generator;
use PHPUnit\Framework\TestCase;

class LeagueCSVReaderTest extends TestCase
{
    use WithShopsProvider;

    private LeagueCSVReader $leagueCSVReader;

    protected function setUp(): void
    {
        parent::setUp();

        $this->leagueCSVReader = new LeagueCSVReader();
    }

    public function testShouldYieldCSVShopDataDTO(): void
    {
        //given
        $filePath = 'tests/resources/test_stores.csv';

        //when
        $result = $this->leagueCSVReader->read($filePath);

        //then
        $this->assertInstanceOf(Generator::class, $result);
        $this->assertEquals($this->getCSVShopDataDTOArray(), iterator_to_array($result));
    }
}
