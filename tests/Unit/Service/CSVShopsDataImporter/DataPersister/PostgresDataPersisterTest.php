<?php

namespace App\Tests\Unit\Service\CSVShopsDataImporter\DataPersister;

use App\Service\CSVShopsDataImporter\DataPersister\PostgresDataPersister;
use App\Tests\Helper\WithShopsProvider;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PostgresDataPersisterTest extends TestCase
{
    use WithShopsProvider;

    private EntityManagerInterface|MockObject $entityManagerMock;

    private PostgresDataPersister $postgresDataPersister;

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);

        $this->postgresDataPersister = new PostgresDataPersister($this->entityManagerMock);
    }

    public function testShouldPersistData(): void
    {
        //given
        $shopsDataDTOsGenerator = $this->getGeneratorWithCSVShopDataDTOs();
        $shopsDataDTOsArray = $this->getShopEntitiesArray();

        $this->entityManagerMock
            ->expects($this->exactly(3))
            ->method('persist')
            ->withConsecutive(
                [$shopsDataDTOsArray[0]],
                [$shopsDataDTOsArray[1]],
                [$shopsDataDTOsArray[2]]
            );

        $this->entityManagerMock->expects($this->once())->method('clear');
        $this->entityManagerMock->expects($this->once())->method('flush');

        //when
        $this->postgresDataPersister->persist($shopsDataDTOsGenerator);
    }
}
