<?php

namespace App\Tests\Feature\API;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\Shop;
use App\Repository\ShopRepository;
use App\Tests\Helper\WithEntityManager;
use Hautelook\AliceBundle\PhpUnit\BaseDatabaseTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ShopsResourceTest extends ApiTestCase
{
    use BaseDatabaseTrait;
    use WithEntityManager;

    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadEntityManager();

        static::populateDatabase();

        $this->client = static::createClient();
    }

    public function testGetShopById(): void
    {
        //given
        $method = 'GET';
        $url = '/api/shops/2';
        $headers = ['headers' => ['accept' => 'application/json']];

        //when
        $this->client->request($method, $url, $headers);

        //then
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertJsonContains([
            'id' => 2,
            'name' => 'Store 2',
            'city' => 'McClureview',
            'street' => 'Durgan Wells 2',
            'phone' => '+1 (410) 657-5305',
            'postalCode' => '45114-1113',
            'salesman' => 'Moses Armstrong',
        ]);
        $this->assertMatchesResourceItemJsonSchema(Shop::class, format: 'json');
    }

    /**
     * @dataProvider provideParametersAndExpectedShops
     */
    public function testGetShopsCollectionByCriteria(string $parameters, array $expectedShopsArray): void
    {
        //given
        $method = 'GET';
        $url = '/api/shops' . $parameters;
        $body = ['headers' => ['accept' => 'application/json']];

        //when
        $this->client->request($method, $url, $body);

        //then
        $this->assertResourceResponse();
        $this->assertJsonContains($expectedShopsArray);
        $this->assertMatchesResourceCollectionJsonSchema(Shop::class, format: 'json');
    }

    /**
     * @dataProvider provideParametersAndExpectedResponse
     */
    public function testResourceShouldBeUpdated(int $id, array $json, Shop $expectedResult): void
    {
        //given
        $method = 'PATCH';
        $url = '/api/shops/' . $id;
        $body = [
            'headers' => ['accept' => 'application/json'],
            'json' => $json,
        ];

        //when
        $this->client->request($method, $url, $body);

        //then
        /** @var ShopRepository $shopRepository */
        $shopRepository = static::getContainer()->get(ShopRepository::class);
        $this->assertEquals($expectedResult, $shopRepository->find($id));

        /** @var NormalizerInterface $normalizer */
        $normalizer = static::getContainer()->get(NormalizerInterface::class);

        $this->assertResourceResponse();
        $this->assertMatchesResourceItemJsonSchema(Shop::class, format: 'json');
        $this->assertJsonContains($normalizer->normalize($expectedResult, 'array'));
    }

    private function assertResourceResponse(): void
    {
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
    }

    private function provideParametersAndExpectedShops(): array
    {
        return [
            [
                '',
                [
                    [
                        'id' => 1,
                        'name' => 'Store 1',
                        'city' => 'New Carley',
                        'street' => 'Ernser Haven 1',
                        'phone' => '+1 (320) 352-7909',
                        'postalCode' => '20721',
                        'salesman' => 'Price Beatty',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Store 2',
                        'city' => 'McClureview',
                        'street' => 'Durgan Wells 2',
                        'phone' => '+1 (410) 657-5305',
                        'postalCode' => '45114-1113',
                        'salesman' => 'Moses Armstrong',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Store 3',
                        'city' => 'New Wittingfort',
                        'street' => 'Schoen Center 6',
                        'phone' => '+1-341-437-0782',
                        'postalCode' => '29669-9212',
                        'salesman' => 'Vivian Gutkowski',
                    ]
                ]
            ],
            [
                '?page=1&city=New',
                [
                    [
                        'id' => 1,
                        'name' => 'Store 1',
                        'city' => 'New Carley',
                        'street' => 'Ernser Haven 1',
                        'phone' => '+1 (320) 352-7909',
                        'postalCode' => '20721',
                        'salesman' => 'Price Beatty',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Store 3',
                        'city' => 'New Wittingfort',
                        'street' => 'Schoen Center 6',
                        'phone' => '+1-341-437-0782',
                        'postalCode' => '29669-9212',
                        'salesman' => 'Vivian Gutkowski',
                    ]
                ]
            ],
            [
                '?page=1&name=1&city=New',
                [
                    [
                        'id' => 1,
                        'name' => 'Store 1',
                        'city' => 'New Carley',
                        'street' => 'Ernser Haven 1',
                        'phone' => '+1 (320) 352-7909',
                        'postalCode' => '20721',
                        'salesman' => 'Price Beatty',
                    ],
                ]
            ],
            [
                '?page=1&name=Store%202',
                [
                    [
                        'id' => 2,
                        'name' => 'Store 2',
                        'city' => 'McClureview',
                        'street' => 'Durgan Wells 2',
                        'phone' => '+1 (410) 657-5305',
                        'postalCode' => '45114-1113',
                        'salesman' => 'Moses Armstrong',
                    ],
                ]
            ],
        ];
    }

    public function provideParametersAndExpectedResponse(): array
    {
        return [
            [
                1,
                [
                    'name' => 'Test',
                ],
                (new Shop())
                    ->setId(1)
                    ->setName('Test')
                    ->setCity('New Carley')
                    ->setStreet('Ernser Haven 1')
                    ->setPhone('+1 (320) 352-7909')
                    ->setPostalCode('20721')
                    ->setSalesman('Price Beatty')
                ,
            ],
            [
                2,
                [
                    'name' => 'Example shop',
                    'city' => 'Lublin',
                ],
                (new Shop())
                    ->setId(2)
                    ->setName('Example shop')
                    ->setCity('Lublin')
                    ->setStreet('Durgan Wells 2')
                    ->setPhone('+1 (410) 657-5305')
                    ->setPostalCode('45114-1113')
                    ->setSalesman('Moses Armstrong')
                ,
            ],
            [
                3,
                [
                    'street' => 'Example street',
                    'phone' => '000 000 000',
                    'postalCode' => '12-345',
                    'salesman' => 'Jan Kowalski',
                ],
                (new Shop())
                    ->setId(3)
                    ->setName('Store 3')
                    ->setCity('New Wittingfort')
                    ->setStreet('Example street')
                    ->setPhone('000 000 000')
                    ->setPostalCode('12-345')
                    ->setSalesman('Jan Kowalski')
                ,
            ]
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->closeEntityManager();
    }
}
