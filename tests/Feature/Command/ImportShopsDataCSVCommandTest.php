<?php

namespace App\Tests\Feature\Command;

use App\Entity\Shop;
use App\Repository\ShopRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ImportShopsDataCSVCommandTest extends KernelTestCase
{
    private CommandTester $commandTester;

    protected function setUp(): void
    {
        parent::setUp();

        $application = new Application(self::bootKernel());
        $command = $application->find('import:csv-shops-data');
        $this->commandTester = new CommandTester($command);
    }

    public function testShouldSaveImportedDataInDb(): void
    {
        //given
        $filePath = 'tests/resources/test_stores.csv';

        //when
        $this->commandTester->execute(['filePath' => $filePath]);

        //then
        /** @var ShopRepository $shopRepository */
        $shopRepository = static::getContainer()->get(ShopRepository::class);
        $this->assertEquals($this->getExpectedEntitiesArray(), $shopRepository->findAll());

        $this->commandTester->assertCommandIsSuccessful();
        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString('Shops data successfully imported to database!', $output);
    }

    public function testShouldNotImportOnWrongFilePath(): void
    {
        //given
        $filePath = 'tests/resources/no_exist_file.csv';

        //when
        $this->commandTester->execute(['filePath' => $filePath]);

        //then
        /** @var ShopRepository $shopRepository */
        $shopRepository = static::getContainer()->get(ShopRepository::class);
        $this->assertEquals([], $shopRepository->findAll());

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString('File not exist in given path!', $output);
    }

    public function testShouldNotImportOnWrongFileStructure(): void
    {
        //given
        $filePath = 'tests/resources/wrong_structure_test_stores.csv';

        //when
        $this->commandTester->execute(['filePath' => $filePath]);

        //then
        /** @var ShopRepository $shopRepository */
        $shopRepository = static::getContainer()->get(ShopRepository::class);
        $this->assertEquals([], $shopRepository->findAll());

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString(
            'App\Entity\Shop::setSalesman(): Argument #1 ($salesman) must be of type',
            $output
        );
    }

    /**
     * @return Shop[]
     */
    private function getExpectedEntitiesArray(): array
    {
        return [
            (new Shop())
                ->setId(1)
                ->setName('Store 1')
                ->setCity('New Carley')
                ->setStreet('Ernser Haven 1')
                ->setPhone('+1 (320) 352-7909')
                ->setPostalCode('20721')
                ->setSalesman('Price Beatty')
            ,
            (new Shop())
                ->setId(2)
                ->setName('Store 2')
                ->setCity('McClureview')
                ->setStreet('Durgan Wells 2')
                ->setPhone('+1 (410) 657-5305')
                ->setPostalCode('45114-1113')
                ->setSalesman('Moses Armstrong')
            ,
            (new Shop())
                ->setId(3)
                ->setName('Store 3')
                ->setCity('New Wittingfort')
                ->setStreet('Schoen Center 6')
                ->setPhone('+1-341-437-0782')
                ->setPostalCode('29669-9212')
                ->setSalesman('Vivian Gutkowski')
            ,
        ];
    }
}
