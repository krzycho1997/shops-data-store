<?php

namespace App\Tests\Integration;

use App\Tests\Helper\WithEntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DBTest extends KernelTestCase
{
    use WithEntityManager;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadEntityManager();
    }

    public function testShouldConnectToDB(): void
    {
        //given
        $this->entityManager->getConnection()->connect();

        //when
        $isConnected = $this->entityManager->getConnection()->isConnected();

        //then
        $this->assertTrue($isConnected);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->closeEntityManager();
    }
}
