#!/usr/bin/env bash

docker-compose exec php php bin/console --env=prod --no-debug "$@"
